package com.example.scavngr.tools;

import android.os.AsyncTask;

import com.example.scavngr.MainActivity;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class APICaller implements Runnable {

    private String route;
    private String data;
    private String requestType;
    private String result;
    private int response;

    public APICaller(String route, String data, String requestType){
        //set context variables if required
        this.route = route;
        this.data = data;
        this.requestType = requestType;
    }

    @Override
    public void run() {
        String url = MainActivity.URLSTRING + route;
        String data = this.data;
        String reqType = requestType;
        try {
            URL urlobj = new URL(url);
            HttpURLConnection conn = (HttpURLConnection)urlobj.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod(reqType);
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.connect();

            DataOutputStream writer = new DataOutputStream(conn.getOutputStream());
            writer.writeBytes(data);
            writer.flush();
            writer.close();

            response = conn.getResponseCode();
            try {
                InputStream inputStream = new BufferedInputStream(conn.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder result = new StringBuilder();
                String line;
                while((line = reader.readLine()) != null) {
                    result.append(line);
                }

                this.result = result.toString();
            }
            catch(IOException e){
                e.printStackTrace();
            }
            finally {
                conn.disconnect();
            }
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }

    public String getResult(){
        return result;
    }

    public int getResponse(){
        return response;
    }
}
