package com.example.scavngr;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.scavngr.R;
import com.example.scavngr.tools.APICaller;
import com.example.scavngr.ui.login.LoginFragment;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class LoginActivity extends AppCompatActivity {

    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //TODO: figure out why I can't find this motherfucker
        final LoginButton loginButton = findViewById(R.id.login);

        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        boolean isLoggedIn = accessToken != null && !accessToken.isExpired();
        if(isLoggedIn){
            Intent homeIntent = new Intent(getBaseContext(), MainActivity.class);
            startActivity(homeIntent);
        }

        callbackManager = CallbackManager.Factory.create();

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                loginButton.setVisibility(View.GONE);
                String facebookID = loginResult.getAccessToken().getUserId();
                String loginJSON = "{\"username\": \"" + facebookID + "\"}";
                APICaller caller = new APICaller("login/", loginJSON, "POST");
                Thread callerThread = new Thread(caller);
                callerThread.start();
                try {
                    callerThread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                int responseCode = caller.getResponse();
                if (responseCode == 200) {
                    // Get hunt if they have one active
                    String huntJSON = caller.getResult();
                    if(huntJSON == null || huntJSON.equals("")){
                        //TODO: Store hunt info locally
                    }
                }

                Intent homeIntent = new Intent(getBaseContext(), MainActivity.class);
                startActivity(homeIntent);
            }

            public void onCancel() {
                AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
                builder.setMessage("Login canceled!");
                AlertDialog dialog = builder.create();
                dialog.show();
            }

            @Override
            public void onError(FacebookException error) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
                builder.setMessage("An unexpected error occurred. Please try again later!");
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }
}
