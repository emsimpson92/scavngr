package com.example.scavngr.ui.signup;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.scavngr.MainActivity;
import com.example.scavngr.R;
import com.example.scavngr.tools.APICaller;
import com.example.scavngr.ui.home.HomeViewModel;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SignupFragment extends Fragment {

    private SignupViewModel signupViewModel;

    private EditText username;
    private EditText password;
    private EditText password2;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        signupViewModel =
                ViewModelProviders.of(this).get(SignupViewModel.class);
        View root = inflater.inflate(R.layout.fragment_signup, container, false);

        Button signup = root.findViewById(R.id.signup2);
        username = root.findViewById(R.id.username);
        password = root.findViewById(R.id.password);
        password2 = root.findViewById(R.id.password2);

        signup.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                String uname = username.getText().toString();
                String pass = password.getText().toString();
                String pass2 = password2.getText().toString();

                if(!pass.equals(pass2)){
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Your passwords don't match!");
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    return;
                }

                // Encrypt the password
                String hash = "";
                try {
                    MessageDigest md = MessageDigest.getInstance("SHA-256");
                    md.update(pass.getBytes());
                    byte[] bytes = md.digest();
                    StringBuilder sb = new StringBuilder();
                    for (byte aByte : bytes) {
                        sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
                    }
                    hash = sb.toString();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
                String signupJSON = "{\"username\": \"" + uname + "\", \"password\": \"" + hash + "\", \"huntsAttempted\": \"0\", \"huntsCompleted\": \"0\"}";

                APICaller caller = new APICaller("signup/", signupJSON, "POST");
                Thread callerThread = new Thread(caller);
                callerThread.start();
                try{
                    callerThread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                int responseCode = caller.getResponse();
                if(responseCode != 201){
                    // Something is borked
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("An unknown error occurred. Please try again later.");
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                else{
                    // Switch to home
                    String result = caller.getResult();
                    if(!result.equals("")){
                        //TODO: store the player JSON that is returned
                    }
                    Intent homeIntent = new Intent(getActivity().getBaseContext(), MainActivity.class);
                    startActivity(homeIntent);
                }

            }
        });

        return root;
    }
}
