package com.example.scavngr.ui.clues;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class CluesViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public CluesViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is clues fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}