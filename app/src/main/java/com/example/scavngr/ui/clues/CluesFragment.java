package com.example.scavngr.ui.clues;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.scavngr.R;

public class CluesFragment extends Fragment {

    private CluesViewModel cluesViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        cluesViewModel =
                ViewModelProviders.of(this).get(CluesViewModel.class);
        View root = inflater.inflate(R.layout.fragment_clues, container, false);
        final TextView textView = root.findViewById(R.id.text_dashboard);
        cluesViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }
}
