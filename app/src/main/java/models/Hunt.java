package models;

public class Hunt {
    public int huntID;
    public String name;
    public String description;
    public double latitude;
    public double longitude;
    public Integer maxPlayers = null;
    public int currentPlayers;

    public Hunt() { }
}
