package models;

public class Clue {
    public int clueID;
    public String clueText;
    public double latitude;
    public double longitude;

    public Clue() { }
}
