package models;

public class Player {
    public int playerID;
    public String username;
    public String password;
    public int huntsAttempted;
    public int huntsCompleted;

    public Player() { }
}
